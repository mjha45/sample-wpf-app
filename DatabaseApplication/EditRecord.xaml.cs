﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DatabaseApplication
{
    /// <summary>
    /// Interaction logic for EditRecord.xaml
    /// </summary>
    public partial class EditRecord : Window
    {
        Customer cust;

        public EditRecord(Customer customer)
        {
            cust = customer;
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // Close the edit window 
            DialogResult = false;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            editID.Text = cust.CustomerID;
            editCompanyName.Text = cust.CompanyName;
            editContactName.Text = cust.ContactName;
            editContactTitle.Text = cust.ContactTitle;
            editCountry.Text = cust.Country;
            editCity.Text = cust.City;
            editRegion.Text = cust.Region;
            editPostalCode.Text = cust.PostalCode;
            editAddress.Text = cust.Address;
            editPhone.Text = cust.Phone;
            editFax.Text = cust.Fax;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            //Ask user for confirmation of edit
            MessageBoxResult editYesNo = MessageBox.Show("Are you sure you want to make these changes?", "Edit Customer", MessageBoxButton.YesNo, MessageBoxImage.Question);

            //If user confirms
            if (editYesNo == MessageBoxResult.Yes)
            {
                //Create an instance of a data connection to the customer DB
                NorthWindDataContextDataContext con = new NorthWindDataContextDataContext();

                //Use lambda function to store the selected customer into a new customer object called custedit
                Customer custedit = con.Customers.First(i => i.CustomerID == cust.CustomerID);

                //Set attributes for custedit to equal the value in the textboxes
                custedit.CompanyName = editCompanyName.Text.Trim();
                custedit.ContactName = editContactName.Text.Trim();
                custedit.ContactTitle = editContactTitle.Text.Trim();
                custedit.Country = editCountry.Text.Trim();
                custedit.City = editCity.Text.Trim();
                custedit.Region = editRegion.Text.Trim();
                custedit.PostalCode = editPostalCode.Text.Trim();
                custedit.Address = editAddress.Text.Trim();
                custedit.Phone = editPhone.Text.Trim();
                custedit.Fax = editFax.Text.Trim();

                //Save changes to DB
                con.SubmitChanges();

                //Confirm update
                MessageBox.Show("Customer " + cust.CustomerID + " has been successfully updated", "Edit Customer", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                //Refresh data grid
                Application app = Application.Current;
                MainWindow main = (MainWindow)app.MainWindow;
                main.refreshGrid();
                DialogResult = true;

            }
            else return;
        }





    }
}
