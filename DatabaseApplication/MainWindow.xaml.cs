﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CustomerDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // Create an instance of a data connection to the database customer    
            // table 
            NorthWindDataContextDataContext con = new NorthWindDataContextDataContext();

            // Create a customer list 
            List<Customer> customers = (from c in con.Customers
                                        select c).ToList();
            CustomerDataGrid.ItemsSource = customers;

        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void AddRecord_Click(object sender, RoutedEventArgs e)
        {
            AddRecord addRecordWindow = new AddRecord();
            addRecordWindow.ShowDialog();
        }

        internal void refreshGrid()
        {
            NorthWindDataContextDataContext con = new NorthWindDataContextDataContext();

            // Create a customer list 
            List<Customer> customers = (from c in con.Customers
                                        select c).ToList();

            // Blank out the items and restore with current data
            CustomerDataGrid.ItemsSource = null;
            CustomerDataGrid.ItemsSource = customers;
        }

        private void DeleteRecord_Click(object sender, RoutedEventArgs e)
        {
            //In case the user doesn't select a record when clicking Delete
            if (CustomerDataGrid.SelectedItem == null)
            {
                MessageBox.Show("Please select a record to delete", "Delete Customer", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //Store selected customer into an object called customer
            Customer customer = (Customer)CustomerDataGrid.SelectedItem;

            //Create an instance of a data connection to the customer DB
            NorthWindDataContextDataContext con = new NorthWindDataContextDataContext();

            //Ask user to confirm their decision to delete a record
            MessageBoxResult deleteYesNo = MessageBox.Show("Are you sure you want to delete " + customer.CustomerID + "?", "Delete Customer", MessageBoxButton.YesNo, MessageBoxImage.Question);

            //If the user chooses Yes
            if (deleteYesNo == MessageBoxResult.Yes)
            {
                //Select customers from DB that match the user's selection
                var cus = (from d in con.Customers where d.CustomerID == customer.CustomerID select d).FirstOrDefault();

                //If a record is retrieved, ensure the delete cascades to any child tables associated with the customer being deleted
                if (cus != null)
                {
                    //Select each orders from Orders table that contain the matching CustomerID being deleted
                    var order = (from d in con.Orders where d.CustomerID == cus.CustomerID select d).ToList();

                    if (order != null)
                    {
                        //Loop through the Orders and delete
                        foreach (Order or in order)
                        {
                            con.GetTable<Order>().DeleteOnSubmit(or);

                            //Loop through and delete each of the Order Details that are associated with each Order being deleted
                            foreach (Order_Detail od in or.Order_Details)
                            {
                                con.GetTable<Order_Detail>().DeleteOnSubmit(od);
                            }
                        }
                    }
                    con.GetTable<Customer>().DeleteOnSubmit(cus);
                    //Save changes to DB
                    con.SubmitChanges();
                }

                //Refresh display
                refreshGrid();
            }
            else return;

        }

        private void EditRecord_Click(object sender, RoutedEventArgs e)
        {
            //In case the user doesn't select a record when clicking Edit
            if (CustomerDataGrid.SelectedItem == null)
            {
                MessageBox.Show("Please select a record to edit", "Edit Customer", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //Store selected customer into an object called customer
            Customer customer = (Customer)CustomerDataGrid.SelectedItem;

            EditRecord editRecordWindow = new EditRecord(customer);
            editRecordWindow.ShowDialog();
        }


    }
}
